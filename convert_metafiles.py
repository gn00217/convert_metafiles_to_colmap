import argparse
import re

# Original IMAGE_PATH, TX, TY, TZ, QW, QX, QY, QZ

# Image list (COLMAP) with two lines of data per image:
#   IMAGE_ID, QW, QX, QY, QZ, TX, TY, TZ, CAMERA_ID, NAME


def parse_args():
    parser = argparse.ArgumentParser(
        description="convert a standard posenet metafiles format to COLMAP image.txt format")

    parser.add_argument("--name", default="images",
                        help="path to the metafile that will be converted")
    parser.add_argument(
        "--input_path", help="path to the metafile that will be converted")
    parser.add_argument(
        "--output_path", help="path to where the converted metafile will be generated")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    metaFile = open(args.input_path)
    convertedMetaFile = open(f"{args.output_path}/{args.name}.txt", 'a')
    lines = metaFile.readlines()
    for count, row in enumerate(lines):
        row = row.split()
        name, x, y, z, qw, qx, qy, qz = row
        searchObj = re.search('([^\/]+$)', name)
        name = searchObj.group()
        newRow = [count, qw, qx, qy, qz, x, y, z, 1, name]
        newRow = ' '.join(str(x) for x in newRow)
        convertedMetaFile.write(newRow)
        convertedMetaFile.write('\n\n')
